db.rooms.insertOne({
	"name": "single",
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": "false"
});

/*name - single
b. accomodates - 2
c. price - 1000
d. description - A simple room with all the basic necessities
e. rooms_available - 10
f. isAvailable - false

*/

db.rooms.insertMany([
		{
			"name": "double",
			"accomodates": 3,
			"price": 2000,
			"description": "A room fit for a small family going on a vacation",
			"rooms_available": 5,
			"isAvailable": "false"
		},
		{
			"name": "queen",
			"accomodates": 4,
			"price": 4000,
			"description": "A room with a queen sized bed perfect for a simple getaway",
			"rooms_available": 15,
			"isAvailable": "false"
		}
	])

/*name - double
b. accomodates - 3
c. price - 2000
d. description - A room fit for a small family going on a vacation
e. rooms_available - 15
f. isAvailable - false

name - queen
b. accomodates - 4
c. price - 4000
d. description - A room with a queen sized bed perfect for a simple getaway
e. rooms_available - 15
f. isAvailable - false

*/

db.rooms.find({
		"name": "double"
	});


db.rooms.updateOne(
	{
		"name": "queen"
	},
	{
		$set: {
			"rooms_available": 0
		}
	}
	)

db.rooms.deleteMany({
	"rooms_available": 0
})